package com.slimeservers.dayz.util;

import com.slimeservers.dayz.DayZ;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.logging.Level;

public class HardwareTag {
    private final String Motherboard_SerialNum;
    private final boolean linux;
    private final boolean windows;

    public boolean isWindows() {
        return windows;
    }

    public boolean isLinux() {
        return linux;
    }

    public HardwareTag() {
        String OSName=  System.getProperty("os.name");
        DayZ.log(Level.WARNING, "Tagging hardware...");
        if(OSName.contains("Windows")){
            Motherboard_SerialNum = fetchWindowsMotherboard_SerialNumber();
            windows = true; linux = false;
            return;
        }
        else{
            Motherboard_SerialNum = fetchLinuxMotherBoard_serialNumber();
            linux = true; windows = false;
            return;
        }
    }

    public String getMotherboard_SerialNum() {
        return Motherboard_SerialNum;
    }

    protected static String fetchWindowsMotherboard_SerialNumber() {
        String result = "";
        try {
            Process p = Runtime.getRuntime().exec("wmic baseboard get serialnumber");

            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result += line;
            }
            input.close();
        }
        catch(Exception E){
            System.err.println("Windows MotherBoard Exp : "+E.getMessage());
        }
        DayZ.log(Level.WARNING, "Motherboard Serial Number: " + result.replace("SerialNumber", " ").trim());
        return result.replace("SerialNumber", " ").trim();
    }


    protected static String fetchLinuxMotherBoard_serialNumber() {
        String command = "dmidecode -s baseboard-serial-number";
        String sNum = null;
        try {
            Process SerNumProcess = Runtime.getRuntime().exec(command);
            BufferedReader sNumReader = new BufferedReader(new InputStreamReader(SerNumProcess.getInputStream()));
            sNum = sNumReader.readLine().trim();
            SerNumProcess.waitFor();
            sNumReader.close();
        }
        catch (Exception ex) {
            System.err.println("Linux Motherboard Exp : "+ex.getMessage());
            sNum =null;
        }
        return sNum;
    }
}
