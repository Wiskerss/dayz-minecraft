package com.slimeservers.dayz.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiYesNoCallback;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.util.Color;

// !!! Called from client proxy only !!!
@SideOnly(Side.CLIENT)
public class DayZMainMenu extends GuiScreen implements GuiYesNoCallback {

    public DayZMainMenu() {
        // Called during CLIENT pre initialization
    }

    @SubscribeEvent
    public void onGuiOpenEvent(GuiOpenEvent event) {
        //if(event.gui instanceof GuiMainMenu) {
        //    event.gui = this;
        //}
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        ScaledResolution scaledResolution = new ScaledResolution(Minecraft.getMinecraft());
        int xAnchor = mc.currentScreen.width/2;
        int yAnchor = mc.currentScreen.height/2;
        drawRect(xAnchor-100, yAnchor-100, xAnchor+100, yAnchor+100, Color.CYAN.hashCode());

    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
    }

    protected void keyTyped(char par1, int par2) {}

    @Override
    public void initGui() {

    }
}
