package com.slimeservers.dayz.proxy;

import com.slimeservers.dayz.client.TickHandler;
import com.slimeservers.dayz.client.item.DayZItemManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class CommonProxy {
    private DayZItemManager itemManager;

    public void registerRenders() {
        itemManager.registerRenders();
    }

    public void preInitialization(FMLPreInitializationEvent e) {
        // Starts the Tick Handler
        MinecraftForge.EVENT_BUS.register(new TickHandler());
        itemManager = new DayZItemManager();

    }

    public void initialization(FMLInitializationEvent e) {
    }

    public void postInitialization(FMLPostInitializationEvent e) {
    }


}
