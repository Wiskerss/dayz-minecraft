package com.slimeservers.dayz.client;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TickHandler {

    @SubscribeEvent
    public void onPlayerTick(TickEvent.PlayerTickEvent tick) {

    }

    @SubscribeEvent
    public void onClientTick(TickEvent.ClientTickEvent tick) {

    }

    @SubscribeEvent
    public void onRenderTick(TickEvent.RenderTickEvent tick) {

    }

    @SubscribeEvent
    public void onWorldTick(TickEvent.WorldTickEvent tick) {
        tick.world.tick();
        tick.phase.compareTo(TickEvent.Phase.START);
    }

    @SideOnly(Side.SERVER)
    @SubscribeEvent
    public void onServerTick(TickEvent.ServerTickEvent tick) {

    }
}
