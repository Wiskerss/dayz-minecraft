package com.slimeservers.dayz.client;

import com.slimeservers.dayz.client.item.DayZItemManager;
import com.slimeservers.dayz.gui.DayZMainMenu;
import com.slimeservers.dayz.proxy.CommonProxy;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SoundCategory;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class ClientProxy extends CommonProxy {

    @Override
    public void preInitialization(FMLPreInitializationEvent e) {
        super.preInitialization(e);
        // Catches Main Menu GUI before its called to replace it with DayZ Menu
        MinecraftForge.EVENT_BUS.register(new DayZMainMenu());
    }

    @Override
    public void initialization(FMLInitializationEvent e) {
        super.initialization(e);
    }

    @Override
    public void postInitialization(FMLPostInitializationEvent e) {
        super.postInitialization(e);
    }

    @Override
    public void registerRenders() {
        super.registerRenders();
        //TODO: Registers renders for all items in the mod --CLIENT SIDE--
        DayZItemManager.registerRenders();
    }
}
