package com.slimeservers.dayz.client.item.enums;

public enum ItemStatus {
    DRENCHED, DAMP, MOIST, DRY, CHARGING, ON, OFF, RAW, COOKED, MOLDY, DRIED;
}
