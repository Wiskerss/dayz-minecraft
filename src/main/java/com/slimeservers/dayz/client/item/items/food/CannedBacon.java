package com.slimeservers.dayz.client.item.items.food;

import com.slimeservers.dayz.client.item.types.DayZFoodItem;

public class CannedBacon extends DayZFoodItem {
    public CannedBacon(boolean canned, int amount, float saturation, boolean isWolfFood) {
        super(true, amount, saturation, isWolfFood);
        this.setUnlocalizedName("Canned Bacon");
        this.setRegistryName("canned_bacon");
    }
}
