package com.slimeservers.dayz.client.item.enums;

import com.slimeservers.dayz.DayZ;
import java.util.logging.Level;

/**
 * Taken from https://dayz.gamepedia.com/Loot #Item Condition
 */
public enum ItemIntegrity {
    Pristine(100),Worn(70),Damaged(40),Badly_Damaged(20),Ruined(0);

    private int health;

    ItemIntegrity(int health) {
        this.health = health;
    }

    public int getHealth() {
        return health;
    }

    public void decreaseHealth() {
        this.health--;
    }

    public void increaseHealth() {
        this.health++;
    }

    // If the integrity of an item were worn and had a health of 64 this function would return
    // the difference between that and the minimum health for pristine which is 71
    // So this would return 71-64 which is 7
    @Deprecated
    public int distanceFromNextIntegrety() {
        return 0;
    }

    static ItemIntegrity getIntegrity(int integrity) {
        if (integrity == 0) return ItemIntegrity.Ruined;
        if (integrity <= 20) return ItemIntegrity.Badly_Damaged;
        if (integrity <= 40) return ItemIntegrity.Damaged;
        if (integrity <= 70) return ItemIntegrity.Worn;
        if (integrity <= 100) return ItemIntegrity.Pristine;
        DayZ.log(Level.WARNING, "item integrity must be a number 1-100");
        return ItemIntegrity.Pristine;
    }

}
