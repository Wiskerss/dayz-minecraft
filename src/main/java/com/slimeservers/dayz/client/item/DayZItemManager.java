package com.slimeservers.dayz.client.item;

import com.google.common.collect.ImmutableList;
import com.slimeservers.dayz.DayZ;
import com.slimeservers.dayz.client.item.items.food.CannedBacon;
import com.slimeservers.dayz.client.item.types.DayZFoodItem;
import com.slimeservers.dayz.client.item.types.DayZItem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.model.ModelLoaderRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.util.ArrayList;
import java.util.List;

public class DayZItemManager {

    // List of all items in minecraft registry
    private List<Item> allItems = ImmutableList.copyOf(Item.itemRegistry);
    private List<DayZFoodItem> dayzFoodItems = new ArrayList<DayZFoodItem>();
    private List<DayZItem> dayzItems = new ArrayList<DayZItem>();

    private static final DayZFoodItem cannedBacon = new CannedBacon(true, 0, 4, true);

    public DayZItemManager() {
        registerItems();
    }

    private void registerItems() {
        GameRegistry.registerItem(cannedBacon, cannedBacon.getRegistryName());
    }

    public static void registerRenders() {
        //ModelLoader.setCustomMeshDefinition();
    }
}

