package com.slimeservers.dayz.client.item.types;

import com.slimeservers.dayz.client.item.enums.ItemIntegrity;
import com.slimeservers.dayz.client.item.enums.ItemStatus;
import net.minecraft.item.Item;

import javax.annotation.Nullable;

public class DayZItem extends Item {

    // Numbers 1-100 only please :(
    private ItemIntegrity itemHealth;

    @Nullable
    private ItemStatus status;

    public DayZItem(ItemIntegrity health, ItemStatus status) {
        this.itemHealth = health;
        this.status = status;
    }

    /**
     * Creates an item with no status. Only use if you don't give a fuck.
     */
    public DayZItem(ItemIntegrity health) {
        this.itemHealth = health;
        this.status = null;
    }

    /**
     * Creates an item in Pristine condition. Only use if you don't give a fuck.
     */
    public DayZItem() {
        this.itemHealth = ItemIntegrity.Pristine;
        this.status = null;
    }

}
