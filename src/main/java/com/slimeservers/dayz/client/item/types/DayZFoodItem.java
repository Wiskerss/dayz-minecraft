package com.slimeservers.dayz.client.item.types;

import com.slimeservers.dayz.client.item.enums.ItemIntegrity;
import net.minecraft.item.ItemFood;

public class DayZFoodItem extends ItemFood {

    private boolean cannedFood;
    private boolean isOpen;

    //Just a way to adjust the randomness
    private int percentageSpawnOpen = 40;

    private ItemIntegrity itemHealth;

    public DayZFoodItem(boolean canned, int amount, float saturation, boolean isWolfFood) {
        super(amount, saturation, isWolfFood);
        this.isOpen = randomBool();
    }

    public DayZFoodItem(boolean canned, boolean isOpen, int amount, float saturation, boolean isWolfFood) {
        super(amount, saturation, isWolfFood);
        this.isOpen = isOpen;
    }

    // 40% chance the can will render as opened, 60% chance of it being closed
    protected boolean randomBool() {
        int rand = (int) ((Math.random() * (1 - 100)) + 1);
        if(rand <= this.percentageSpawnOpen) {
            return true;
        }
        return false;
    }

    public void setPercentageSpawnOpen(int percentageSpawnOpen) {
        this.percentageSpawnOpen = percentageSpawnOpen;
    }
}
