package com.slimeservers.dayz;

import com.slimeservers.dayz.proxy.CommonProxy;
import com.slimeservers.dayz.util.HardwareTag;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.*;

import java.util.logging.Level;
import java.util.logging.Logger;

@Mod(name = DayZ.MOD_STRING, modid = DayZ.MOD_STRING, version = DayZ.MOD_VERSION)
public class DayZ {

    // Current list of mods loaded
    private static ModContainer modContainer;

    // FIXME: MOD INFO
    public static final String MOD_STRING = "DayZ";
    public static final String MOD_VERSION = "development";

    // FIXME: PROXIES
    private static final String CLIENT_PROXY = "com.slimeservers.dayz.client.ClientProxy";
    private static final String SERVER_PROXY = "com.slimeservers.dayz.proxy.CommonProxy";
    @SidedProxy(serverSide = SERVER_PROXY, clientSide = CLIENT_PROXY)
    private static CommonProxy proxy;

    @Mod.Instance(MOD_STRING)
    public static DayZ instance;

    private static Logger log = Logger.getLogger(DayZ.class.getName());

    private static HardwareTag hardwareIdentifier;

    @Mod.EventHandler
    public static void preInitialization(FMLPreInitializationEvent e) {
        hardwareIdentifier = new HardwareTag();
        proxy.preInitialization(e);
    }

    @Mod.EventHandler
    public static void initialization(FMLInitializationEvent e) {
        proxy.registerRenders();
        proxy.initialization(e);
    }

    @Mod.EventHandler
    public static void postInitialization(FMLPostInitializationEvent e) {
        proxy.postInitialization(e);
    }

    public static void log(Level level, String str) {
        log.log(level, "[DayZ] " + str);
        return;
    }

    public static HardwareTag getHardwareIdentifier() {
        return hardwareIdentifier;
    }

    public static CommonProxy getProxy() {
        return proxy;
    }

    public static DayZ getInstance() {
        return instance;
    }
}
